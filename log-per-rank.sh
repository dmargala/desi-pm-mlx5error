#!/bin/bash
# Wrapper script to redirect output to a file

LOGFILE=$1.${SLURM_NODEID}.${SLURM_LOCALID}

# consume first argument
shift

# empty contents of log file
> $LOGFILE

# copy stdout to logfile
exec &> >(tee -a $LOGFILE)

exec "$@"

