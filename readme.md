# desi-pm-mlx5error



## Initial setup

```
ssh perlmutter

cd $SCRATCH
git clone git@gitlab.com:dmargala/desi-pm-mlx5error.git
cd desi-pm-mlx5error

module load PrgEnv-gnu
module load python

#note: you'll need to adjust prefix to use a directory within your project, such as `/global/common/software/nvendor`
conda env create -f env-conda.yml --prefix /global/common/software/dasrepo/dmargala/mlx5error

source activate /global/common/software/dasrepo/dmargala/mlx5error

cat env-pip-extra.txt | xargs -l pip install

MPICC="cc -shared" pip install --force --no-cache-dir --no-binary=mpi4py mpi4py
```

## Usage after setup

```
ssh perlmutter

cd $SCRATCH/desi-pm-mlx5error

module load PrgEnv-gnu
module load python

source activate /global/common/software/dasrepo/dmargala/mlx5error

#note, you may need to adjust the account used in batch.sh, it is currently m1759_g
sbatch batch.sh
```

## Job output

Job output will be located at 
```
$SCRATCH/debug-mlx5error-$SLURM_JOB_ID
```

## Launch many jobs

```
ssh perlmutter

cd $SCRATCH/desi-pm-mlx5error

module load PrgEnv-gnu
module load python

source activate /global/common/software/dasrepo/dmargala/mlx5error

sbatch -N 120 batch.sh
sbatch -N 120 batch.sh
sbatch -N 120 batch.sh
sbatch -N 120 batch.sh
```


Jobid 73458 had the mlx5 error:

```
mlx5: nid002365: got completion with error:
00000000 00000000 00000000 00000000
00000000 00000000 00000000 00000000
00000002 00000000 00000000 00000000
00000000 1f006802 0000853b 0080a0d2
Fatal Python error: Segmentation fault

Current thread 0x00007f801dc58380 (most recent call first):
  File "/pscratch/sd/d/dmargala/desi-pm-mlx5error/mpi-reproducer.py", line 50 in fake_proc
  File "/pscratch/sd/d/dmargala/desi-pm-mlx5error/mpi-reproducer.py", line 147 in process_night_tile
  File "/pscratch/sd/d/dmargala/desi-pm-mlx5error/mpi-reproducer.py", line 252 in map
  File "/pscratch/sd/d/dmargala/desi-pm-mlx5error/mpi-reproducer.py", line 291 in main
  File "/pscratch/sd/d/dmargala/desi-pm-mlx5error/mpi-reproducer.py", line 295 in <module>
srun: error: nid002365: task 0: Segmentation fault
```

Jobid 73619 had a different an error at MPI import:

```
Traceback (most recent call last):
  File "/pscratch/sd/d/dmargala/desi-pm-mlx5error/mpi-reproducer.py", line 295, in <module>
    main()
  File "/pscratch/sd/d/dmargala/desi-pm-mlx5error/mpi-reproducer.py", line 263, in main
    from mpi4py import MPI
ImportError: /opt/cray/pe/lib64/libmpi_gnu_91.so.12: cannot read file data: Input/output error
```


## Log

| JobID | NumNodes | mlx5 error | import error | 
| ------| -------- | ---------- | ------------ |
| 73409 |  60  |  no |  no |
| 73457 |  60  |  no |  no |
| 73458 |  60  | yes |  no |
| 73459 |  30  |  no |  no |
| 73460 | 120  |  no |  no |
| 73462 | 120  |  no |  no |
| 73463 | 120  |  no |  no |
| 73464 |  30  |  no |  no |
| 73465 |  30  |  no |  no |
| 73466 |  60  |  no |  no |
| 73613 | 120  |  no |  no |
| 73614 | 120  |  no |  no |
| 73615 | 120  |  no |  no |
| 73616 | 120  |  no |  no |
| 73617 | 120  |  no |  no |
| 73618 | 120  |  no |  no |
| 73619 | 120  |  no | yes |
| 73620 | 120  |  no |  no |
| 73621 | 120  |  no |  no |
| 73622 |   5  |  no |  no |
| 73623 | 120  |  no |  no |
| 73651 | 120  |  no |  no |
| 73652 | 240  |  no |  no |
| 73653 | 480  |  no | yes | 
| 73672 | 120  |  no |  no |
| 73676 | 120  |  no |  no |


### misc notes


```
# check for mlx5 error
> grep 'mlx5:' *out

# check for import error:
> grep /opt/cray/pe/lib64/libmpi_gnu_91.so.12 *out | cut -d':' -f 1 | sort | uniq

# get sacct info
> ls -1 *out | cut -d'-' -f 2 | cut -d'.' -f 1 | while read -r i; do echo $i \| $(sacct -j $i.0 -o AllocNodes -n); done
```
