#!/usr/bin/env python

### scale_run imports
import argparse
from dataclasses import dataclass, field
import datetime
import os
import subprocess
### scale_run imports

### fake_proc imports
import time
start_imports = time.time()
import sys

import desispec.scripts.proc as proc
import desispec.io
from desispec.io import findfile
from desispec.util import runcmd
from desiutil.log import get_logger
import desiutil.iers
from desispec.workflow.desi_proc_funcs import update_args_with_headers
from desispec.workflow.exptable import validate_badamps
stop_imports = time.time()
### fake_proc imports

def fake_proc(args, comm):

    log = get_logger()

    #- Use the provided comm to determine rank and size
    rank = comm.rank
    size = comm.size

    #- Freeze IERS after parsing args so that it doesn't bother if only --help
    desiutil.iers.freeze_iers()

    #- Preflight checks
    if rank > 0:
        #- Let rank 0 fetch these, and then broadcast
        args, hdr, camhdr = None, None, None
    else:
        args, hdr, camhdr = update_args_with_headers(args)

    ## Make sure badamps is formatted properly
    if rank == 0 and args.badamps is not None:
        args.badamps = validate_badamps(args.badamps)

    args = comm.bcast(args, root=0)
    hdr = comm.bcast(hdr, root=0)
    camhdr = comm.bcast(camhdr, root=0)

    comm.barrier()

    #- What are we going to do?
    if rank == 0:
        log.info('----------')
        log.info('Input {}'.format(args.input))
        log.info('Night {} expid {}'.format(args.night, args.expid))
        log.info('Obstype {}'.format(args.obstype))
        log.info('Cameras {}'.format(args.cameras))
        log.info('Output root {}'.format(desispec.io.specprod_root()))
        log.info('----------')

    #- Create output directories if needed
    if rank == 0:
        preprocdir = os.path.dirname(findfile('preproc', args.night, args.expid, 'b0'))
        expdir = os.path.dirname(findfile('frame', args.night, args.expid, 'b0'))
        os.makedirs(preprocdir, exist_ok=True)
        os.makedirs(expdir, exist_ok=True)

    #- Wait for rank 0 to make directories before proceeding
    comm.barrier()

    #- Assemble fibermap for science exposures
    fibermap = None
    fibermap_ok = None
    if rank == 0 and args.obstype == 'SCIENCE':
        fibermap = findfile('fibermap', args.night, args.expid)
        if not os.path.exists(fibermap):
            tmp = findfile('preproc', args.night, args.expid, 'b0')
            preprocdir = os.path.dirname(tmp)
            fibermap = os.path.join(preprocdir, os.path.basename(fibermap))

            log.info('Creating fibermap {}'.format(fibermap))
            cmd = 'assemble_fibermap -n {} -e {} -o {}'.format(
                    args.night, args.expid, fibermap)
            if args.badamps is not None:
                cmd += ' --badamps={}'.format(args.badamps)
            runcmd(cmd, inputs=[], outputs=[fibermap])

        fibermap_ok = os.path.exists(fibermap)

    comm.barrier()

    #- If assemble_fibermap failed and obstype is SCIENCE, exit now
    fibermap_ok = comm.bcast(fibermap_ok, root=0)

    comm.barrier()

    if args.obstype == 'SCIENCE' and not fibermap_ok:
        sys.stdout.flush()
        if rank == 0:
            log.critical('assemble_fibermap failed for science exposure; exiting now')
        sys.exit(13)

    #- Wait for rank 0 to make fibermap if needed
    fibermap = comm.bcast(fibermap, root=0)

    comm.barrier()

    for i in range(rank, len(args.cameras), size):
        camera = args.cameras[i]
        outfile = findfile('preproc', args.night, args.expid, camera)
        outdir = os.path.dirname(outfile)
        cmd = "desi_preproc -i {} -o {} --outdir {} --cameras {}".format(
            args.input, outfile, outdir, camera)
        if args.scattered_light :
            cmd += " --scattered-light"
        if fibermap is not None:
            cmd += " --fibermap {}".format(fibermap)
        if not args.obstype in ['ARC'] : # never model variance for arcs
            if not args.no_model_pixel_variance :
                cmd += " --model-variance"
        runcmd(cmd, inputs=[args.input], outputs=[outfile])

    comm.barrier()

@dataclass
class NightTileTask:
    """Represents a single task to process a unique night-tile"""
    night: str
    tileid: str
    expids: list
    camword: str
    weight: float
    name: str


def process_night_tile(comm, task):
    for expid in task.expids:
        cmd = f'proc --traceshift --night {task.night} --cameras {task.camword} --nostdstarfit --nofluxcalib --expid {expid}'
        #- parse command string through argument parser
        cmdargs = cmd.split()[1:]
        args = proc.parse(cmdargs)
        #- run command using provided MPI communicator
        fake_proc(args, comm)

    if comm.rank == 0:
        outdir = os.path.join(os.environ.get('DESI_SPECTRO_REDUX'), os.environ.get('SPECPROD'))
        for expid in task.expids:
            subprocess.check_output(f"find {outdir} -name '*{expid}*fits' -delete", shell=True)


@dataclass
class TaskPartition():
    tasks: list = field(default_factory=list)
    weight: float = 0
    scale_factor: float = 1

    def score(self):
        """Returns the score of the partition"""
        return (self.weight*self.scale_factor, self.scale_factor)

    def potential_score(self, task):
        """Returns the score including if task were added to this partition"""
        return ((self.weight + task.weight)*self.scale_factor, self.scale_factor)

    def add_task(self, task):
        """Appends task and updates weight"""
        self.tasks.append(task)
        self.weight += task.weight


def partition_tasks(tasks, num_partitions, scale_factors=None):
    """Divides the provided tasks into the specified number of partitions"""
    #- Sort tasks based on weight (highest to lowest)
    ordered_tasks = sorted(tasks, key=lambda x: x.weight, reverse=True)
    #- If scale_factors is not provided assume they are equal
    if scale_factors is None:
        scale_factors = [1]*num_partitions
    #- Initialize partitioned task lists
    partitions = [TaskPartition(scale_factor=x) for x in scale_factors]
    for task in ordered_tasks:
        #- Identify "lightest" partition to receive task
        partition = min(partitions, key=lambda p: p.potential_score(task))
        #- Add task to a partition
        partition.add_task(task)
    return partitions


class Slots():

    def __init__(self, comm, nodes_per_slot=1):
        
        self.comm = comm
        self.nodes_per_slot = nodes_per_slot
        self.tasks = None

        self.node = os.environ.get('SLURMD_NODENAME')
        #- SLURM_NODEID is node index
        self.node_index = int(os.environ.get('SLURM_NODEID'))
        #- SLURM_LOCALID is local node rank
        self.node_rank = int(os.environ.get("SLURM_LOCALID"))
        #- TODO: this is not great. srun doesn't change this env variable
        self.node_size = int(os.environ.get("SLURM_NTASKS_PER_NODE"))

        self.nodes = sorted(comm.allgather(self.node))
        self.nnodes = len(set(self.nodes))

        #- nnodes must be divisible by nodes_per_slot
        assert self.nnodes % nodes_per_slot == 0, f"invalid {nodes_per_slot=} with {self.nnodes=}"

        #- assign ranks to slots
        self.nslots = self.nnodes // nodes_per_slot
        self.slot_size = nodes_per_slot * self.node_size
        self.slot_rank = comm.rank % self.slot_size
        self.slot_index = self.node_index // nodes_per_slot
        #- Split comm into groups for each slot
        self.intra_slot_comm = comm.Split(color=self.slot_index, key=self.slot_rank)
        #- Split comm into two groups:
        #-   the first group includes rank 0 of each intra_slot_comm and is used to scatter tasks across slots.
        #-   the second group includes all other ranks but is not used for anything.
        self.inter_slot_comm = comm.Split(color=self.slot_rank, key=self.slot_index)

        #- Ignore for now
        self.slot_scale_factors = None


    def distribute(self, tasks):
        #- Partition tasks into slots
        if self.comm.rank == 0:
            partitions = partition_tasks(tasks, self.nslots, self.slot_scale_factors)
            partitioned_tasks = [p.tasks for p in partitions]
        else:
            partitioned_tasks = None

        #- Scatter tasks among slots
        if self.slot_rank == 0:
            scattered_tasks = self.inter_slot_comm.scatter(partitioned_tasks, root=0)
        else:
            scattered_tasks = None
        #- Broadcast tasks within a slot
        self.tasks = self.intra_slot_comm.bcast(scattered_tasks, root=0)

    def map(self, func):
        '''Applies func to tasks uses each slots' communicator'''
        for task in self.tasks:
            #- Apply func to task
            if self.intra_slot_comm.rank == 0:
                print(f"Group {self.slot_index} starting task: {task}", flush=True)
            func(self.intra_slot_comm, task)


def main():

    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument("--task-list", type=str, default="night-tile-tasks.txt", help="list of tasks")
    parser.add_argument("--nodes-per-slot", type=int, default=5, help="number of nodes per processing slot")
    args = parser.parse_args()

    #- Initialize MPI
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    # comm.Set_errhandler(MPI.ERRORS_ARE_FATAL)

    #- Create list of tasks on rank 0
    if comm.rank == 0:
        tasks = []
        with open(args.task_list, 'r') as infile:
            for line in infile:
                words = line.strip().split()
                night, tileid, expids, camword, weight, name = words
                task = NightTileTask(night, tileid, list(map(int, expids.split(','))), camword, float(weight), name)
                tasks.append(task)
        print(f"read {len(tasks)} from {args.task_list}")
    else:
        tasks = None

    # if comm.rank == 0:
    #     print(tasks)
    #     with open(args.save_task_list, 'w') as outfile:
    #         for task in tasks:
    #             outfile.write(f"{task.night} {task.tileid} {','.join(map(str, task.expids))} {task.camword} {task.weight} {task.name}\n")

    #- Create slots for task processing
    slots = Slots(comm, nodes_per_slot=args.nodes_per_slot)
    #- Distribute tasks among slots
    slots.distribute(tasks)
    #- Process tasks
    slots.map(process_night_tile)


if __name__ == "__main__":
    main()
