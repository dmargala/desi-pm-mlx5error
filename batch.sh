#!/bin/bash -l
#SBATCH --constraint=gpu
#SBATCH --nodes=60
#SBATCH --tasks-per-node=64
#SBATCH --cpus-per-task=2
#SBATCH --time=5
#SBATCH --qos=regular
#SBATCH --output=slurm-%j.out
#SBATCH --account=m1759_g






#- Write pipeline outputs to Perlmutter scratch
NAME=debug-mlx5error-$SLURM_JOB_ID
OUTDIR=$SCRATCH/$NAME
if [ -d $OUTDIR ]; then
    echo removing existing $OUTDIR
    rm -rf $OUTDIR
fi
mkdir -p $OUTDIR

#- Setup pipeline env vars
ORIG_DESI_SPECTRO_REDUX=$DESI_SPECTRO_REDUX
export DESI_SPECTRO_REDUX=$SCRATCH
export SPECPROD=$NAME
ln -s $ORIG_DESI_SPECTRO_REDUX/everest/calibnight $DESI_SPECTRO_REDUX/$SPECPROD/calibnight
export DESI_LOGLEVEL=CRITICAL

#- OpenMP Settings
#- https://www.openmp.org/spec-html/5.1/openmpch6.html
export OMP_NUM_THREADS=1

#- Python settings
#- https://docs.python.org/3/using/cmdline.html#environment-variables
export PYTHONFAULTHANDLER=1

#- MPICH settings
# export MPICH_GPU_SUPPORT_ENABLED=1

#- libfabric settings 
# export FI_LOG_LEVEL=info

#- Create a unique directory for log files
mkdir -p $OUTDIR/log
LOGFILE=$OUTDIR/log/slurm-$SLURM_JOB_ID.out

#- timeout after 4 minutes
timeout 240 srun --tasks-per-node=64 --cpus-per-task=2 --cpu-bind=cores ./log-per-rank.sh $LOGFILE ./mpi-reproducer.py

find $OUTDIR -name '*.fits' -delete
